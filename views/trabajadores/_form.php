<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trabajadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaNacimiento')->widget(DatePicker::className(), [
        'options' => [
            'class'=>'form-control',
            'placeholder'=>'Fecha Nacimiento'
        ],
        'clientOptions'=>[
            'dateFormat' => 'dd-mm-yy',
            'language' => 'es'
        ],
    ]) ?>


    <?= $form->field($model, 'foto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'delegacion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
