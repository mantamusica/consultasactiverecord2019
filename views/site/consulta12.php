<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;
$this->title = 'Consultas';
?>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <h2><?= $consulta ?></h2>
    <?= ListView::widget([
        'dataProvider' => $datos,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'itemView' => '_consulta12',
    ]);
    ?>

</div>

