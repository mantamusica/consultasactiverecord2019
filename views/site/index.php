<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Consultas';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('.center {display: flex;justify-content: center; .size_font {font-size:8px;}');
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-12 center">
                <h1>CONSULTAS</h1>
            </div>
            <div class="col-lg-12 center">
                <h3 class="lead">BLOQUE INICIAL DE CONSULTAS</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 center">
                <p><?= Html::a('MOSTRAR TODOS LOS TRABAJADORES VAR_DUMP ', ['/site/consulta_12a'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('MOSTRAR TODAS LAS DELEGACIONES (CON ACTIVERECORD)', ['/site/consulta_14a'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('MOSTRAR TODAS LAS DELEGACIONES (CON COMMAND) ', ['/site/consulta_14b'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('MOSTRAR TODAS LAS DELEGACIONES (CON QUERY BUILDER) ', ['/site/consulta_14c'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR LAS DELEGACIONES CUYA POBLACION SEA GEELONG ACTIVERECORD ', ['/site/consulta_17a1'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR LAS DELEGACIONES CUYA POBLACION SEA GEELONG COMMAND', ['/site/consulta_17a2'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR LOS TRABAJADORES DE LA DELEGACION 1 ACTIVERECORD ', ['/site/consulta_17b1'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR LOS TRABAJADORES DE LA DELEGACION 1 COMMAND', ['/site/consulta_17b2'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR LOS TRABAJADORES ORDENADOS POR NOMBRE ACTIVERECORD ', ['/site/consulta_17c1'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR LOS TRABAJADORES ORDENADOS POR NOMBRE COMMAND', ['/site/consulta_17c2'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR LOS TRABAJADORES QUE NO CONOZCO LA FECHA DE NACIMIENTO ACTIVERECORD ', ['/site/consulta_17d1'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR LOS TRABAJADORES QUE NO CONOZCO LA FECHA DE NACIMIENTO COMMAND', ['/site/consulta_17d2'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR LAS DELEGACIONES QUE TENGAN UNA POBLACION DISTINTA A GEELONG Y DE LAS CUALES SEPAMOS LA DIRECCION', ['/site/consulta_21a'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR TODOS LOS TRABAJADORES DE LAS DELEGACIONES DE GEELONG ', ['/site/consulta_21b'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR LOS TRABAJADORES Y LAS DELEGACIONES DE LOS TRABAJADORES QUE TENGAN FOTO ', ['/site/consulta_21c'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('SACAR LAS DELEGACIONES QUE NO TIENEN TRABAJADORES', ['/site/consulta_21d'], ['class'=>'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-12 center">
                <p><?= Html::a('LISTAR EL NOMBRE Y LOS APELLIDOS DE TODOS LOS TRABAJADORES CONCATENADOS Y DENOMINAR AL CAMPO “NOMBRE_COMPLETO”', ['/site/consulta_23'], ['class'=>'btn btn-success']) ?></p>
            </div>
        </div>
    </div>
</div>
