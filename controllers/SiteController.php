<?php

namespace app\controllers;

use app\models\Delegacion;
use app\models\Trabajadores;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use \yii\db\Query;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionConsulta_12a()
    {
        /* Active Record */
        $consulta=Trabajadores::find();

        $d=new ActiveDataProvider([
            "query"=>$consulta,
            'pagination' => [
                'pageSize' => 1,
            ],

        ]);


        return $this->render("consulta12",[
            "datos"=>$d,
            "consulta"=>'MOSTRAR TODOS LOS TRABAJADORES',
        ]);

    }
    public function actionConsulta_14a()
    {
        /* Active Record */
        $listado=Delegacion::find()->all();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'MOSTRAR TODAS LAS DELEGACIONES ',
        ]);
    }
    public function actionConsulta_14b()
    {
        /* Create command */
        $listado=Yii::$app->db
            ->createCommand("select * from delegacion")
            ->queryAll();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'MOSTRAR TODAS LAS DELEGACIONES ',
        ]);
    }
    public function actionConsulta_14c()
    {
        /* Query Builder */
        $listado= (new Query())
            ->select("*")
            ->from("trabajadores")
            ->all();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'MOSTRAR TODAS LAS DELEGACIONES ',
        ]);
    }
    public function actionConsulta_17a1()
    {
        /* Active Record */
        $listado=Delegacion::find()
            ->where(['like','poblacion','Geelong'])
            ->all();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'LISTAR LAS DELEGACIONES CUYA POBLACION SEA GEELONG ',
        ]);
    }
    public function actionConsulta_17a2()
    {
        /* Create command */
        $listado=Yii::$app->db
            ->createCommand("select * from delegacion where poblacion like 'Geelong'")
            ->queryAll();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'LISTAR LAS DELEGACIONES CUYA POBLACION SEA GEELONG ',
        ]);

    }
    public function actionConsulta_17b1()
    {
        /* Active Record */
        $listado=Trabajadores::find()
            ->where(['=','delegacion',1])
            ->all();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'LISTAR LOS TRABAJADORES DE LA DELEGACION 1 ',
        ]);
    }
    public function actionConsulta_17b2()
    {
        /* Create command */
        $listado=Yii::$app->db
            ->createCommand("select * from trabajadores where delegacion = 1")
            ->queryAll();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'LISTAR LOS TRABAJADORES DE LA DELEGACION 1 ',
        ]);

    }
    public function actionConsulta_17c1()
    {
        /* Active Record */
        $listado=Trabajadores::find()
            ->orderBy('nombre')
            ->all();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'LISTAR LOS TRABAJADORES ORDENADOS POR NOMBRE ',
        ]);
    }
    public function actionConsulta_17c2a()
    {
        /* Create command */
        $listado=Yii::$app->db
            ->createCommand("select * from trabajadores order by nombre")
            ->queryAll();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'LISTAR LOS TRABAJADORES ORDENADOS POR NOMBRE ',
        ]);

    }
    public function actionConsulta_17d1()
    {
        /* Active Record */
        $listado=Trabajadores::find()
            ->where('fechaNacimiento IS NULL')
            ->all();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'LISTAR LOS TRABAJADORES QUE NO CONOZCO LA FECHA NACIMIENTO ',
        ]);
    }
    public function actionConsulta_17d2()
    {
        /* Create command */
        $listado=Yii::$app->db
            ->createCommand("select * from trabajadores where fechaNacimiento IS NULL")
            ->queryAll();

        return $this->render("consulta02",[
            "datos"=>$listado,
            "consulta"=>'LISTAR LOS TRABAJADORES QUE NO CONOZCO LA FECHA NACIMIENTO ',
        ]);

    }
    public function actionConsulta_21a()
    {
        /* Active Record */
        $listado=Delegacion::find()
            ->where(['and', 'direccion IS NOT NULL', ['!=', 'poblacion', 'Geelong']])
            ->all();

        return $this->render("consulta03",[
            "datos"=>$listado,
            "consulta"=>'LISTAR LAS DELEGACIONES QUE TENGAN UNA POBLACION DISTINTA A SANTANDER Y DE LAS CUALES SEPAMOS LA DIRECCION',
        ]);
    }
    public function actionConsulta_21b()
    {

        /* Active Record */
        $listado=Trabajadores::find()
            ->leftJoin('delegacion','delegacion.id=trabajadores.delegacion')
            ->where(['poblacion'=>'Geelong'])
            ->all();

        return $this->render("consulta03",[
            "datos"=>$listado,
            "consulta"=>'LISTAR TODOS LOS TRABAJADORES DE LAS DELEGACIONES DE GEELONG',
        ]);
    }
    public function actionConsulta_21c()
    {

        /* Active Record */
        $listado=Trabajadores::find()
            ->leftJoin('delegacion','delegacion.id=trabajadores.delegacion')
            ->where('foto IS NOT NULL')
            ->all();

        return $this->render("consulta03",[
            "datos"=>$listado,
            "consulta"=>'LISTAR LOS TRABAJADORES Y LAS DELEGACIONES DE LOS TRABAJADORES QUE TENGAN FOTO',
        ]);
    }
    public function actionConsulta_21d()
    {

        /* Active Record */
        $listado=Delegacion::find()
            ->leftJoin('trabajadores','delegacion.id=trabajadores.delegacion')
            ->where('trabajadores.delegacion IS NULL')
            ->all();

        return $this->render("consulta03",[
            "datos"=>$listado,
            "consulta"=>'SACAR LAS DELEGACIONES QUE NO TIENEN TRABAJADORES',
        ]);
    }
    public function actionConsulta_23()
    {

        /* Active Record */
        $listado=Trabajadores::find()
            ->all();

        return $this->render("consulta04",[
            "datos"=>$listado,
            "consulta"=>'SACAR LAS DELEGACIONES QUE NO TIENEN TRABAJADORES',
        ]);
    }
}
